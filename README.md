**FINAL PROJECT TOPIK KHUSUS KELAS-B**
==========================

* 05111740000002 Bintang Nuralamsyah 
* 05111740000010 Isnaini Nurul KurniaSari 
* 05111740000014 Risky Aswi Narni 
* 05111740000168 Zahrul Zizki Dinanto


*1.1 **Konsep***
---------------
* [ ]  Judul Paper : BP-AODV: Blackhole Protected AODV Routing Protocol for MANETs Based on Chaotic Map 
* [ ]  Penulis     : ALY M. EL-SEMARY
* [ ]  Tahun Terbit: Received June 18, 2019, accepted July 10, 2019, date of publication July 15, 2019, date of current version August 1, 2019.
* [ ]  Sumber      : https://ieeexplore.ieee.org/abstract/document/8763958

*1.2* ***Abstraksi***
-------------------

* [ ]  AODV adalah routing protocol yang kini digunakan secara luas pada MANET ( Mobile Ad-hoc Network ).
* [ ]  AODV rentang terhadap Blackhole Attack
* [ ]  SAODV adalah routing protocol pengembangan dari AODV yang dapat melindungi jaringan dari Blackhole Attack
* [ ]  Kelemahan SAODV adalah SAODV tidak dapat menangani Cooperative Blackhole Attack
* [ ]  AODV adalah routing protocol yang kini digunakan secara luas pada MANET ( Mobile Ad-hoc Network ). AODV rentang terhadap Blackhole Attack
* [ ]  SAODV adalah routing protocol pengembangan dari AODV yang dapat melindungi jaringan dari Blackhole Attack
* [ ]  Kelemahan SAODV adalah SAODV tidak dapat menangani Cooperative Blackhole Attack
* [ ]  Paper ini mengajukan BP - AODV protocol, dimana BP – AODV protocol dapat mengatasi blackhole attack dan cooperative blackhole attack, baik saat route discovery atau saat data forwarding


*1.3 **Introduction***
----------------------

* Mobile Ad-hoc Network ( MANET ) adalah perangkat / node yang dapat mengorganisir dirinya sendiri, yang berhubungan secara langsung antar perangkat tanpa adanya infrastruktur yang tetap.

* Node bekerja sebagai host dan router

* AODV adalah AODV adalah routing protocol yang termasuk kategori reactive routing protocol. Seperti reactive routing protocol lainnya, AODV hanya menyimpan informasi routing seputar path dan host yang aktif. Didalam AODV, informasi routing disimpan di semua node. Setiap node menyimpan tabel routing next-hop, dimana menyimpan informasi tujuan ke hop berikutnya dimana node tersebut memiliki route tertentu. Didalam AODV, ketika node asal ingin mengirim packet ke tujuan namun tidak ada route yang tersedia, node tersebut akan memulai proses route discovery.

* Cara Kerja :
    1. Didalam proses route discovery, node asal membroadcast route request (RREQ) packets dimana disetakan nomor sequence tujuan. 
    2.  Ketika node tujuan atau node yang memiliki route ke tujuan menerima packet RREQ, node tersebut akan memeriksa nomor sequence tujuan yang sampai pada nodenya ketika packet tiba dan nomor sequence sama didalam RREQ. 
    3.  Untuk memastikan bahwa packet tersebut masih bersifat baru, node tujuan membalas paket RREQ dengan route reply (RREP) packet. RREP dibuat dan dikirim kembail ke node asal hanya jika nomor sequence tujuan sama dengan atau lebih besar dari yang dispesifikasikan di RREQ.
    4.  AODV hanya menggunakan link yang bersifat simetris dan RREP mengikuti path sebaliknya dari path yang dihasilkan oleh RREQ. Ketika menerima RREP, setiap node diantara asal dan tujuan mengupdate routing table next-hop dengan RREP ke tujuannya.
    5.  Node asal kemudian memilih route dengan jumlah hop paling sedikit untuk mengirimkan packet tujuannya.

* Routing Protocol yang digunakan oleh MANET dapat diklasifikasikan menjadi :

    1. proactive
       * Disebut juga sebagai table-driven protocol
       * Setiap node memiliki satu atau banyak routing table yang mencakup semua node yang ada di jaringan.
       * Secara rutin, setiap node akan mempropagandakan routing table nya jika terdapat perubahan di topologi jaringan untuk menjaga topologi agar tetap up to date.
       * Tipe topologi ini memiliki delay yang yang rendah namun kesulitan dalam menghadapi skalabilitas.
       Contoh : DSDV, SDSDV, OLSR, PSR.
    
    2. Reactive
       * Kelas routing protocol hanya membuat route ketika sebuah node di jaringan ingin mengirimkan data ke node lain. Tipe ini memiliki delay yang besar namun tetap menjaga skalabilitas.
       Contoh : DSR, SRP, AODV, SAODV.
    
    3. Hybrid 
       * Akan membagi jaringan ke dalam zona-zona.
       * Di dalam zona tersebut protokol ini mengimplementasikan proactive protokol.
       * Antar zona, protokol ini mengimplementasikan reactive protokol.
       * Hybrid memanfaatkan kelebihan di protokol reactive dan proaktif, namun memerlukan energi ekstra untuk menjaga zona.
       

*1.4 **Pembahasan***
------------------
* Paper ini mengajukan Blackhole Protected – AODV ( BP – AODV) dimana dengan menggunakan chaotic map, protocol ini mampu melindungi jaringan dari blackhole attack dan cooperative blackhole attack 
  Protocol ini akan melindungi jaringan dari node yang bertingkah mencurigakan saat route discorvery atau saat data forwarding
  Dengan menggabungkan pola yang berbeda, setiap pasangan node yang berbeda akan memiliki secret parameter setiap kali route request.

* **Tipe massage:**
  1. MRREQ (Modefied Route REQuest)
  2. MRREP (Modefied Route REPly)
  3. RERR (Route ERRor)
  4. HELLO (HELLO)
  5. RCON (Route CONfirm)
 
    ![1](/uploads/535989453e6a34800f7b2b42b372349a/1.png)

    ![2](/uploads/01427993108bba4b8f7fc4324dcb3670/2.png)
    
* **Tahap penemuan rute dari protokol  BP-AODV dicapai dengan menyelesaikan tiga tahapan atau proses:**
    1. Request
    2. Reply
    3. Confirm
    
    ![3](/uploads/55e447d69ff1c8d2cb83768ace2c95aa/3.png)
    
    ![4](/uploads/d6e611658282586df670dd94d84695d5/4.png)
    
* **Secara khusus, yang src menciptakan pesan MRREQ bersama-sama  dengan  menempatkan nilai-nilai parameter kemudian src menyiarkan pesan MRREQ Ke Tetangganya**
    
    ![5](/uploads/25d84523d9b032564198651585be701e/5.png)

    ![6](/uploads/c22b8f291807666338d5b4ea9465e893/6.png)
    

* **Ketika node perantara menerima MRREQmessage,Pesan akan diabaikan jika sebelumnya diterima oleh forwarding node yang sama,atau jumlah Pesan diterima sebelumnya dari node yang berbeda lebih besar dari tiga. Node menengah melakukan empat langkah yaitu :**
    1. increment jumlah pesan MRREQ diterima
    2. Membangun rute sebaliknya dengan ssrc menggunakan forwarding node
    3. Menyimpan nilai tantangan cs ke dalam   tabel routing
    4. Jika simpul menengah bukanlah dst dan MRREQ yang sebelumnya tidak disiarkan, node akan menambahkan jumlah hop hc termasuk dalam pesan dan menyiarkan ke node tetangganya

    
    ![7](/uploads/d8fcd703b881a0ab4ad7e2f3d052f038/7.png)
    
    ![8](/uploads/bbf637bc5bd77a93c1a8acce8e2ce0f6/8.png)
    
    ![9](/uploads/49eb728a994536d10554234fb1536f0f/9.png)
    
*  **Jika Node Dst pertama kali mendapatkan MRREQ :**
    1. Akan Membuat Nilai-nilai baru
    2. Jika Sudah pernah Mendapat MRREQ Sebelumnya
    3. Akan mengupdate t
    4. Membuat MMREP form dari dst ke src
    5. Mendapatkan v 
    6. DST menciptakan pesan MRREP yang sesuai dan set up parameter pesan yang diperlukan termasuk Nilai dari v bersama dengan menjaga nilai-nilai rahasia η, r 1, dan r 2
    7. DST unicasts pesan MRREP ke src melalui forwarding simpul
    
    ![11](/uploads/8f53511e3cc0730e4cfad05c5cddd55b/11.png)

* **Persamaan Persamaan untuk mendapatkan nilai pada MRREQ :**

    ![12](/uploads/d0eec7b5c232015b81a31c20fb29b311/12.png)    ![13](/uploads/4aa93e11b8c042fe6891954ce4127157/13.png)
    
    ![14](/uploads/9ce7ba4c138fb499b3c7a6979d62cc6a/14.png)    ![15](/uploads/fa8cbafafdce98fc34c714c39b76ad64/15.png)


*   **Ketika node perantara menerima pesan MRREP, itu melakukan empat langkah utama:**
    1. Menetapkan rute sebaliknya ke dst melalui forwarding simpul. simpul diperbolehkan untuk membuat hingga tiga rute.
    2. Node menempatkan rute didirikan menjadi negara tunggu (yaitu, rute tidak dapat digunakan untuk meneruskan paket data sampai berubah menjadi keadaan operasional). Sebuah rute berubah menjadi negara operasional oleh Confirm Proses hanya jika menjamin bahwa rute tidak memiliki simpul berbahaya di jalan.
    3. Node menyimpan nilai v ke dalam tabel routing dengan rute terkait.
    4. Jika simpul menengah adalah src, mengabaikan pesan. Jika tidak, node unicasts setiap pesan MRREP yang diterima memiliki nilai yang sama dari v ke src melalui rute yang berbeda

* **Berikut ini adalah gambar untuk Confirm Round :**
    ![16](/uploads/82293ba334711349b8b1c3781fe4796b/16.png)

* **Confirm Proses akan dipicu oleh destination ketika nilai timer t mencapai 0 :**

    ![17](/uploads/4120cba3bcd6b71b0c416e416c34e253/17.png)

* Secara khusus, ketika nilai timer t mencapai nol, dst menciptakan RCONsesuai pesan yang mencakup nilai-nilai rahasia η, r 1, dan r 2. Selanjutnya, dst menyiarkanpesan RCON ke kelenjar tetangganya ketika nilai timer t mencapai nol, dst menciptakan RCON sesuai pesan yang mencakup nilai-nilai rahasia η, r 1, dan r 2. Selanjutnya, dst menyiarkan pesan RCON ke kelenjar tetangganya.
      
* Ketika sebuah simpul menengah yang tidak pada rute jalan (node 1, 3, 4,7, 8 atau 11 pada Gambar. 6 c) menerima satu set pesan RCON dari node yang berbeda dalam periode waktu tertentu, membandingkan nilai-nilai yang sesuai dari η,r 1, dan r 2.
  Jika mayoritas pesan RCON memiliki nilai yang sesuai sama, node menyiarkan hanya satu pesan mayoritas tersebut. 
  Jika tidak, Jika jumlah pesan RCON memiliki nilai yang sama netral, node mengabaikan pesan.

*  Jika nilai-nilai v m dan v saya adalah sama, node ternyata saya th rute ke keadaan operasional. Jika tidak,node mengumumkan bahwa forwarding simpul yang terkait dengan rute urutan ke-I adalah node berbahaya 
    dan kemudian menghapus rute ke-I dari tabel routing. Terakhir, jika node bukan src, itu menyiarkan salah satu yang paling cocok dengan v m untuk node tetangganya. 
    Jika tidak, node (yaitu, src) mulai meneruskan paket data ke dst berdasarkan proses forwarding yang dikembangkan dibahas pada bagian berikutnya.

* **BP-AODV FORWARDING**
Dimulai oleh src setelah menyetujui rute-nya dengan dst selama proses routing. Selama proses forwarding meneruskan paket data ke dst, itu memilih hop berikutnya n.


*1.5 **Kesimpulan***
-------------------
1. Dapat disimpulkan bahwa AODV memiliki kinerja yang sangat baik dibawah serangan bebas   (ketika blackhole melakukan 3 skenario yang telah disebutkan).
2. Dapat disimpulkan bahwa protokol SAODV sangat baik dibawah serangan bebas dan serangan blackhole yang dihasilkan dengan satu node berbahaya selama skenario pertama.
3. Disisi lain, ketika node berbahaya tidak menempa paket ACK sebagaimana dalam kategori ketiga, maka akan terdeteksi dan terhindar oleh node sumber seperti yang telah diungkapkan hasilnya.
4. Pada akhirnya, hasil menyetujui bahawa protokol BP_AODV protokol memiliki kinerja sangat baik selama serangan bebas dan serangan blackhole selama terdapat tiga skenario yang mendasari.











